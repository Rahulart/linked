import React from 'react'
import Connection from '../components/Connection'
import Header from '../components/Header'
import Home from '../components/Home'

export default function connection() {
  return (
    <div>
        <Header/>
        <Home/>
        <Connection/>
    </div>
  )
}
