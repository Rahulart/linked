import React from 'react'
import Home from '../components/Home'
import Dashboard from '../components/Dashboard'
import Header from '../components/Header'

export default function dashboard() {
  return (
    <div>
        <Header/>
        <Home/>
        <Dashboard/>
    </div>
  )
}
