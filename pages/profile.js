import React from 'react'
import Header from '../components/Header'
import Home from '../components/Home'
import Profile from '../components/Profile'

export default function profile() {
  return (
    <div>
        <Header/>
        <Home/>
        <Profile />
    </div>
  )
}
