// import React from 'react'

// export default function Header() {
//   return (
//     <>
//         <div>

//         </div>
//     </>
//   )
// }


import * as React from 'react';
import PropTypes from 'prop-types';
import AppBar from '@mui/material/AppBar';
import Toolbar from '@mui/material/Toolbar';
import Typography from '@mui/material/Typography';
import useScrollTrigger from '@mui/material/useScrollTrigger';
import Box from '@mui/material/Box';
import IconButton from '@mui/material/IconButton';
import PeopleIcon from '@mui/icons-material/People';
import WorkIcon from '@mui/icons-material/Work';
import MessageIcon from '@mui/icons-material/Message';
import MenuIcon from '@mui/icons-material/Menu';
import MoreHorizIcon from '@mui/icons-material/MoreHoriz';
import { styled } from '@mui/material/styles';
import InputBase from '@mui/material/InputBase';
import HomeIcon from '@mui/icons-material/Home';
import LinkedInIcon from '@mui/icons-material/LinkedIn';
import SearchIcon from '@mui/icons-material/Search';
import NotificationsIcon from '@mui/icons-material/Notifications';
import SettingsOutlinedIcon from '@mui/icons-material/SettingsOutlined';
import PlaylistAddOutlinedIcon from '@mui/icons-material/PlaylistAddOutlined';
import EventIcon from '@mui/icons-material/Event';
import { Link, List, ListItemButton, ListItemText } from '@mui/material';
import {useState} from 'react';
// import { isAutheticated } from './Auth/auth';

import Button from '@mui/material/Button';
// import { styled } from '@mui/material/styles';
import Dialog from '@mui/material/Dialog';
import DialogTitle from '@mui/material/DialogTitle';
import DialogContent from '@mui/material/DialogContent';
import DialogActions from '@mui/material/DialogActions';
// import IconButton from '@mui/material/IconButton';
import CloseIcon from '@mui/icons-material/Close';
import Grid from '@mui/material/Grid';
import AccountCircle from '@mui/icons-material/AccountCircle';
// import { isAutheticated, signout } from './Auth/auth';

import { useRouter } from 'next/router';


const Search = styled('div')(({ theme }) => ({
    position: 'relative',
    borderRadius: theme.shape.borderRadius,
    backgroundColor: '#d9d9d9',//alpha(theme.palette.common.white, 0.15),
    '&:hover': {
      backgroundColor: '#d9d9d9',//alpha(theme.palette.common.white, 0.25),
    },
    marginRight: theme.spacing(2),
    marginLeft: 0,
    width: '100%',
    [theme.breakpoints.up('sm')]: {
      marginLeft: theme.spacing(3),
      width: 'auto',
    },
  }));


  const SearchIconWrapper = styled('div')(({ theme }) => ({
    padding: theme.spacing(0, 2),
    height: '100%',
    position: 'absolute',
    pointerEvents: 'none',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
  }));

  const StyledInputBase = styled(InputBase)(({ theme }) => ({
    color: 'inherit',
    '& .MuiInputBase-input': {
      padding: theme.spacing(1, 1, 1, 0),
      // vertical padding + font size from searchIcon
      paddingLeft: `calc(1em + ${theme.spacing(4)})`,
      transition: theme.transitions.create('width'),
      width: '100%',
      [theme.breakpoints.up('md')]: {
        width: '20ch',
      },
    },
  }));

  

function ElevationScroll(props) {
  const { children, window } = props;
  // Note that you normally won't need to set the window ref as useScrollTrigger
  // will default to window.
  // This is only being set here because the demo is in an iframe.
  const trigger = useScrollTrigger({
    disableHysteresis: true,
    threshold: 0,
    target: window ? window() : undefined,
  });

  return React.cloneElement(children, {
    elevation: trigger ? 4 : 0,
  });
}

ElevationScroll.propTypes = {
  children: PropTypes.element.isRequired,
  /**
   * Injected by the documentation to work in an iframe.
   * You won't need it on your project.
   */
  window: PropTypes.func,
};







export default function Header(props) {
//   const router = useRouter();

    const [opens, setOpens] = useState(false);
    // const {username,email} = isAutheticated();

  const handleClickOpen = () => {
    setOpens(true);
  };
  const handleCloses = () => {
    setOpens(false);
  };

// function signsout(){

//   signout(()=>{
//     router.push('/')
//     alert("Signout Successfully");
//   })
  
// }

  const BootstrapDialog = styled(Dialog)(({ theme }) => ({
    '& .MuiDialogContent-root': {
      padding: theme.spacing(2),
    },
    '& .MuiDialogActions-root': {
      padding: theme.spacing(1),
    },
  }));
    return (
    <>
      <Box sx={{ flexGrow: 1 }} style={{backgroundColor:'#b3b3b3'}}>
      <ElevationScroll {...props}>
        <AppBar style={{height:'4rem',backgroundColor:'#ffffff'}}>
           {/* style={{display:'flex',justifyContent:'space-between'}} */}
          <Toolbar>
            {/* <div> */}
            <Link href='/home'>
            <IconButton edge="start" color="inherit" aria-label="menu" sx={{ mr: 1.5 }}
                style={{padding: "4px"}}
            >
                <LinkedInIcon style={{color:'#2952a3',fontSize:'3rem'}} />
            </IconButton>
            </Link> 

            
          <Search>
            <SearchIconWrapper>
              <SearchIcon />
            </SearchIconWrapper>
            <StyledInputBase
              placeholder="Search"
              //inputProps={{ 'aria-label': 'search' }}
            />
          </Search>
          {/* </div> */}
            <p style={{marginLeft:'38rem'}}></p>
          {/* <div> */}
            <IconButton edge="start" color="inherit" aria-label="menu" sx={{ mr: 3 }}>
                <HomeIcon style={{color:'#666666',fontSize:'2rem'}}/>
            </IconButton>
            <Link href='/connection'>
            <IconButton edge="start" color="inherit" aria-label="menu" sx={{ mr: 3 }}>
                <PeopleIcon style={{color:'#666666',fontSize:'1.9rem'}}/>
            </IconButton>
            </Link>
            <IconButton edge="start" color="inherit" aria-label="menu" sx={{ mr: 3 }}>
                <WorkIcon style={{color:'#666666',fontSize:'1.6rem'}}/>
            </IconButton>
            <IconButton edge="start" color="inherit" aria-label="menu" sx={{ mr: 3 }}>
                <MessageIcon style={{color:'#666666',fontSize:'1.5rem'}}/>
            </IconButton>
            <IconButton edge="start" color="inherit" aria-label="menu" sx={{ mr: 3 }}>
                <NotificationsIcon style={{color:'#666666',fontSize:'1.7rem'}}/>
            </IconButton>

            <IconButton
                size="large"
                edge="end"
                aria-label="account of current user"
                // aria-controls={menuId}
                aria-haspopup="true"
                onClick={handleClickOpen}
                color="inherit"
              >
                <AccountCircle fontSize="large" style={{color:"#666666"}}/>
                {/* {user && <img style={{borderRadius:'50%',width:"50px",height:"50px"}} src={user.picture}/>}  */}
              </IconButton>
              {/* </div> */}
          </Toolbar>
        </AppBar>
      </ElevationScroll>
      </Box>


      <BootstrapDialog
        onClose={handleCloses}
        aria-labelledby="customized-dialog-title"
        open={opens}
        // style={{marginLeft:"65rem"}}
      >
        <Header id="customized-dialog-title" onClose={handleCloses}>
          Modal title
        </Header>

        <DialogContent dividers  style={{backgroundColor:"#ffffff",width:'15rem'}}>
        <div style={{ display: "flex", justifyContent: "space-between" }}>
            <lable>Profile</lable>
            <Button className="head" onClick={handleCloses}>
              X
            </Button>
            
          </div>
          <Grid container justifyContent="center">
          <AccountCircle fontSize="large" style={{color:'#666666'}}/>
            {/* {user && <img style={{borderRadius:'50%',width:"80px",height:"80px"}} src={user.picture}/>} */}
          </Grid>
          <br />         
          <Typography variant="body2" color="text.secondary" align="center"> Name : {"username"}</Typography>
          <Typography variant="body2" color="text.secondary" align="center"> Email : {"email"}</Typography>
         <br/>
         <Grid container justifyContent="center"  style={{border:'1px solid #3399ff', borderRadius:'1rem', padding:'3px 0px'}}>
          <Link  href="/profile" underline="none"  color="primary" style={{ alignItems: "center",margin:"auto",fontSize:'0.9rem'}}>
            View Profile
          </Link>
         </Grid>
         <hr/>
         <Grid>
          <Link underline="none"  color="#000000" style={{ alignItems: "center",margin:"auto",fontSize:'1rem'}}>
            Account
          </Link>
          <Grid color="#666666" style={{ alignItems: "center",margin:"auto",marginTop:'5px',fontSize:'0.9rem'}}>
            <div>Settings</div>
            <div>Help</div>
            <div>Language</div>
          </Grid>
         </Grid>
         <hr/>
         <Grid>
          <Link href="./signin" underline="none"  color="#666666" style={{ alignItems: "center",margin:"auto",fontSize:'0.9rem'}}>
            Sign Out
          </Link>
         </Grid>
         {/* <Grid container justifyContent="center">
          <Link  href="/" underline="none"  color="primary" style={{ alignItems: "center",margin:"auto"}}>
          View Profile
           </Link>
           <span style={{ margin:"1rem"}}>|</span>
           onClick={signsout}
          <Link href="/" underline="none"  color="#ff5722" style={{ alignItems: "center",margin:"auto"}}>
          Sign Out
          </Link>
         </Grid> */}
      {/* <Typography variant="body2" color="text.secondary" align="center" > My Orangisations</Typography>

      <List component="nav" aria-label="secondary mailbox folder"  >
        <ListItemButton >
          <ListItemText primary="Oraganisation Name:" />
        </ListItemButton>

      </List> */}
         
        </DialogContent>
        <DialogActions  style={{backgroundColor:"#ff3333"}}>
      
          <Button autoFocus onClick={handleCloses} style={{ alignItems: "center",width: "100%",margin:"auto",color:"#ffffff", height:"20px"}} >
            Close
          </Button>
        
        </DialogActions>
      </BootstrapDialog>
    

    </>
  );
}
