import React from 'react'
import Box, { BoxProps } from '@mui/material/Box';


function Item(props) {
    const { sx, ...other } = props;
    return (
      <Box
        sx={{
          p: 1,
          m: 1,
          bgcolor: (theme) => (theme.palette.mode === 'dark' ? '#101010' : 'grey.100'),
          color: (theme) => (theme.palette.mode === 'dark' ? 'grey.300' : 'grey.800'),
          border: '1px solid',
          borderColor: (theme) =>
            theme.palette.mode === 'dark' ? 'grey.800' : 'grey.300',
          borderRadius: 2,
          fontSize: '0.875rem',
          fontWeight: '700',
          ...sx,
        }}
        {...other}
      />
    );
  }

export default function Dashboard() {
  return (
    <>
        <div style={{ width: '100%' }}>
        <Box style={{ marginTop:'3rem' }}
            sx={{ display: 'flex', p: 4, borderRadius: 1 }}
        >
            <Item sx={{ flexGrow: 1 }}>
               
            </Item>
            <Item sx={{ flexGrow: 3 }}>Item 2</Item>
            <Item sx={{ flexGrow: 1.8 }}>Item 3</Item>
        </Box>
    </div>
    </>
  )
}
