import React from 'react'
import linkedinlogo from '../images/linkedinlogo.png'
import Typography from '@mui/material/Typography';
import Box from '@mui/material/Box';
import TextField from '@mui/material/TextField';
import Button from '@mui/material/Button';
import Image from 'next/image'
import Link from 'next/link';


export default function SignIn() {
    

  return (
    <>
        <div>
            <div>
                <div style={{width:"7rem",height:"4.5rem",marginLeft:"3rem"}}>
                    <Image src={linkedinlogo} alt="logo"  />
                </div>
            </div>
            <div>
                <form style={{width:'30%',backgroundColor:'#ffffff',margin:'auto',color:'#000000',boxShadow:'0 0 20px rgba(0,0,0,0.5)',borderRadius:'4px'}}>
                    <div style={{padding:'20px'}}>
                        <Typography variant="h5">
                            <b>Sign in</b>
                        </Typography>
                        <Typography variant="caption" display="block">
                        Stay updated on your professional world
                        </Typography>
                        <br/>
                        <Box
                            component="form"
                            sx={{
                                '& > :not(style)': { m: 1, width: '100%' },
                            }}
                            autoComplete="off"
                            >
                            <TextField label="Email" variant="outlined" type={'email'} required/>
                            <TextField label="Password" variant="outlined" type={'password'} required/>
                        </Box>
                        <br/>
                        <Button type='submit' variant="contained" style={{textTransform:'none',width:'100%',borderRadius:'8px',margin:'auto',alignItems:'center'}}>
                        <Link href='./dashboard'>Sign in</Link>
                        </Button>
                        <br/>
                        <br/>
                        {/* <Typography display="block" style={{color:'grey',fontSize:'15px',textAlign:'center'}}>
                        or
                        </Typography>
                        <br/>
                        <Button variant="outlined" style={{textTransform:'none',width:'100%',borderRadius:'8px',margin:'auto',alignItems:'center'}}>
                        
                            Sign in with Google
                        
                        </Button> */}
                        
                    </div>
                
                </form>
                <div style={{textAlign:'center'}}>
                <br/><br/>
                    <Typography variant="h6"  style={{width:'30%',backgroundColor:'#ffffff',margin:'auto'}}>
                        New to LinkedIn? 
                        {" "}
                        <u>
                        <Link href='./signup' >Join now</Link>
                        </u>
                    </Typography>
                    
                </div>
            </div>
        </div>
    </>
  )
}
