import React from 'react'
import MUIDataTable from "mui-datatables";
import  { useState } from "react";
import { useRouter } from "next/router";

import axios from "axios"
import useSWR from 'swr'

import { ThemeProvider } from "@mui/styles";
import { createTheme } from "@mui/material/styles";

export default function Connection() {

    const router =useRouter();
    const [post,setPost]=useState([]);
   //  useEffect(()=>{
    //  const {data,error} = useSWR('challans',fetcher)
     // setPost(data)
//      const arr = []
//      data && data.map((e=> arr.push({"DATE":e.delivery_challan_date,"DELIVER CHALLAN #":e._id.slice(0,5),"REFERENCE NUMBER":e.reference,"CUSTOMER NAME":e.customer_name,"STATUS":"DRAFT","AMOUNT":e.total,"ID":e._id})))
//      if(error) return "An Error has Occured"
//    if(!data) return "Loading"
   

//     const columns = [
//         { name: "DATE"  },
//         { name:"DELIVER CHALLAN #"},
//         { name: "REFERENCE NUMBER"  },
//         { name: "CUSTOMER NAME"  },
      
//         { name:"STATUS"},
//         { name:"AMOUNT"},
      
      
//         {
//           name: "ID",
//           options: {
//             display: false,
//           }}
        
        
//       ];
      

// const options = {
//     onTableChange: (action, state) => {
//       console.log(action);
//       console.dir(state);
//     },
//     print:false,
//     download:false,
//     onRowClick: (rowData) => {
//       router.push(`/challan/${rowData[6]}`)
//               console.log("RowClicked->", rowData[4] );
//           },
//     responsive: 'scroll',
//   };
  
const [responsive, setResponsive] = useState("vertical");
  const [tableBodyHeight, setTableBodyHeight] = useState("400px");
  const [tableBodyMaxHeight, setTableBodyMaxHeight] = useState("");
  const [searchBtn, setSearchBtn] = useState(true);
  const [downloadBtn, setDownloadBtn] = useState(false);
  const [printBtn, setPrintBtn] = useState(false);
  const [viewColumnBtn, setViewColumnBtn] = useState(true);
  const [filterBtn, setFilterBtn] = useState(true);

  const columns = [
    { name: "NAME", options: { filterOptions: { fullWidth: true } } },
    { name: "CURRENT COMPANY" },
    { name: "SCHOOL"  },
    { name: "LOCATION"  },
  ];

  const options = {
    search: searchBtn,
    download: downloadBtn,
    print: printBtn,
    viewColumns: viewColumnBtn,
    filter: filterBtn,
    filterType: "dropdown",
    responsive,
    tableBodyHeight,
    tableBodyMaxHeight,
    onTableChange: (action, state) => {
      console.log(action);
      console.dir(state);
    }
  };


const data = [
    ["Brain Fin", "Codingmart","Kit","Cbe"],
    [
      "Karthi KR",
      "Infosys",
      "Xyz",
      "Cbe"
    ],
    ["Gokul K", "Godigit", "Gct", "Erode"],
    ["Gowtham D", "Godigit", "Psg", "Chennai"],
    ["Saravana K", "Myntra", "Poiuy", "Tpr"],
    ["Rahul R", "Codingmart", "Kce", "Cbe"],
    ["Balu R", "Abfrl", "Ghips", "Salem"],
    ["Suresh K", "Amazon", "Kce", "Erode"],
    ["Nitin C", "Infomatica", "Skcet", "Tpr"],
    ["Akilesh P", "Wipro", "Rec", "chennai"],
    ["Sai Haran", "Abfrl", "Bacet", "Cbe"]
  ];

  return (
    <>
    <div>
        <div style={{marginTop:"6rem",  marginLeft:"2rem", marginRight:"2rem" , display:"fixed"}}>
      {/* <Button variant="contained" style={{marginLeft:"65%",position:"absolute",zIndex:"1",marginTop:"1rem",backgroundColor:"green",color:"white",padding:"5px 20px"}} 
      onClick={() => { router.push('/adddeliverychallans') }}
      >+ New</Button> */}
    <ThemeProvider theme={createTheme()} >
   
        <MUIDataTable
          title={"Search Connection"}
          data={data}
          columns={columns}
          options={options}
        />
    </ThemeProvider>
    </div>
    </div>
    </>
    )
}
