import React from 'react'
import GlobalStyles from "@mui/material/GlobalStyles";
import { ThemeProvider, createTheme } from "@mui/material/styles";

const theme = createTheme({
    palette: {
      background: {
        default: "#ff00ff"
      }
    }
  });

export default function Home() {
  return (
    <ThemeProvider theme={theme}>
      <GlobalStyles
        styles={{
          body: { backgroundColor: "#bfbfbf" }
        }}
      />
      
    </ThemeProvider>
  )
}
