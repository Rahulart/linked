import { Button, Grid } from '@mui/material'
import React from 'react'
import IconButton from '@mui/material/IconButton';
import AccountCircle from '@mui/icons-material/AccountCircle';
import Typography from '@mui/material/Typography';
import EditIcon from '@mui/icons-material/Edit';
import TextField from '@mui/material/TextField';
import Dialog from '@mui/material/Dialog';
import DialogActions from '@mui/material/DialogActions';
import DialogContent from '@mui/material/DialogContent';
import DialogContentText from '@mui/material/DialogContentText';
import DialogTitle from '@mui/material/DialogTitle';
import WorkIcon from '@mui/icons-material/Work';
import SchoolIcon from '@mui/icons-material/School';

export default function Profile() {

  const [open, setOpen] = React.useState(false);

  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };


  return (
    <>
      <div>
        <Grid style={{marginTop:'7rem',backgroundColor:'#ffffff',marginLeft:'4rem',marginRight:'4rem',borderRadius:'1rem'}}>
          <div style={{backgroundImage:`url("https://i.pinimg.com/736x/5e/cf/6b/5ecf6bd8170f61a20560a7332d8c7b91.jpg")`, backgroundSize: '100% 300px',backgroundRepeat:'no-repeat', borderRadius:'1rem'}}>
          <IconButton
                //size="large"
                edge="end"
                aria-label="account of current user"
                // aria-controls={menuId}
                aria-haspopup="true"
                //onClick={handleClickOpen}
                color="inherit"
              >
                <AccountCircle style={{color:'#666666',fontSize:'10rem'}}/>
                {/* {user && <img style={{borderRadius:'50%',width:"50px",height:"50px"}} src={user.picture}/>}  */}
              </IconButton>
          </div>
          <div>
            <div style={{marginLeft:'2rem', marginTop:'1.5rem'}}>
              <div style={{display:'flex', justifyContent:'space-between'}}>
                <div>
                  <Typography variant="h5" color="#000000"><b>Rahul R</b></Typography>
                </div>
                <div>
                  {/* <IconButton edge="start" color="inherit" aria-label="menu" sx={{ mr: 3 }}> */}
                    
                    <div>
                      <Button onClick={handleClickOpen}>
                        <EditIcon style={{color:'#666666',fontSize:'1.6rem'}}/>
                      </Button>
                      <Dialog open={open} onClose={handleClose} style={{width:'100%',margin:'auto'}}>
                        <DialogTitle>Edit Profile</DialogTitle>
                        <DialogContent>
                              <TextField
                              autoFocus
                              margin="dense"
                              id="name"
                              label="Headline*"
                              type="text"
                              fullWidth
                              // variant="standard"
                              />
                              <TextField
                              autoFocus
                              margin="dense"
                              id="name"
                              label="Position"
                              type="text"
                              fullWidth
                              // variant="standard"
                              />
                              <TextField
                              autoFocus
                              margin="dense"
                              id="name"
                              label="Industry"
                              type="text"
                              fullWidth
                              // variant="standard"
                              />
                              <TextField
                              autoFocus
                              margin="dense"
                              id="name"
                              label="Company name*"
                              type="text"
                              fullWidth
                              // variant="standard"
                              />
                              <TextField
                              autoFocus
                              margin="dense"
                              id="name"
                              // label="Company start date*"
                              type="date"
                              fullWidth
                              // variant="standard"
                              />
                              <TextField
                              autoFocus
                              margin="dense"
                              id="name"
                              // label="Company end date"
                              type="date"
                              fullWidth
                              // variant="standard"
                              />
                              <TextField
                              autoFocus
                              margin="dense"
                              id="name"
                              label="Education"
                              type="text"
                              fullWidth
                              // variant="standard"
                              />
                              <TextField
                              autoFocus
                              margin="dense"
                              id="name"
                              // label="Education start date*"
                              type="date"
                              fullWidth
                              // variant="standard"
                              />
                              <TextField
                              autoFocus
                              margin="dense"
                              id="name"
                              // label="Education end date"
                              type="date"
                              fullWidth
                              // variant="standard"
                              />
                              <TextField
                              autoFocus
                              margin="dense"
                              id="name"
                              label="Location"
                              type="text"
                              fullWidth
                              // variant="standard"
                              />
                              <TextField
                              autoFocus
                              margin="dense"
                              id="name"
                              label="City"
                              type="text"
                              fullWidth
                              // variant="standard"
                              />
                              <TextField
                              autoFocus
                              margin="dense"
                              id="name"
                              label="Skills"
                              type="text"
                              fullWidth
                              // variant="standard"
                              />
                              <TextField
                              autoFocus
                              margin="dense"
                              id="name"
                              label="Certification"
                              type="text"
                              fullWidth
                              // variant="standard"
                              />
                              <TextField
                              autoFocus
                              margin="dense"
                              id="name"
                              label="Description"
                              type="text"
                              fullWidth
                              // variant="standard"
                              />
                          
                        </DialogContent>
                        <DialogActions>
                          <Button onClick={handleClose} style={{textTransform:'none',backgroundColor:'#ffffff',color:'#666666',marginTop:'0.5rem',border:'1px solid #666666'}}><b>Cancel</b></Button>
                          <Button onClick={handleClose} style={{textTransform:'none',backgroundColor:'#0073e6',color:'#ffffff',marginTop:'0.5rem',marginRight:'0.7rem'}}><b>Save</b></Button>
                        </DialogActions>
                      </Dialog>
                    </div>
                  {/* </IconButton> */}
                </div>
              </div>
              <Typography variant="body2" color="#000000" style={{marginTop:'0.5rem'}}>Product Engineer Intern at Codingmart Technologies</Typography>
              <Typography variant="body2" color="#666666" style={{marginTop:'0.1rem'}}>Coimbatore, Tamil Nadu, India.</Typography>
              <Typography variant="body2" color="primary" style={{marginTop:'0.7rem'}}><b>222 connections</b></Typography>
              <div>
                <Button style={{textTransform:'none',backgroundColor:'#0073e6',color:'#ffffff', borderRadius:'4rem',marginTop:'0.5rem',marginRight:'0.7rem'}}><b>Open to</b></Button>
                <Button style={{textTransform:'none',backgroundColor:'#ffffff',color:'#0073e6', borderRadius:'4rem',marginTop:'0.5rem',marginRight:'0.7rem',border:'1px solid #0073e6'}}><b>Add Profile Section</b></Button>
                <Button style={{textTransform:'none',backgroundColor:'#ffffff',color:'#666666', borderRadius:'4rem',marginTop:'0.5rem',border:'1px solid #666666'}}><b>More</b></Button>
              </div>
              <br/>
            </div>
          </div>
        </Grid>
        <Grid style={{marginTop:'1.5rem',backgroundColor:'#ffffff',marginLeft:'4rem',marginRight:'4rem',borderRadius:'1rem'}}>
          <br/>
            <div style={{marginLeft:'2rem', marginTop:'1.5rem'}}>
              <div>
              <Typography variant="h5" color="#000000"><b>Experience</b></Typography>
              </div>
              
              <br/>
              <Grid container direction="row" alignItems="center">
                <Grid item>
                  <WorkIcon  style={{color:'#666666',fontSize:'2rem',marginRight:'1rem'}}/>
                </Grid>
                <Grid item>
                  <Typography variant="body2" color="#000000" style={{marginTop:'0.5rem'}}><b>Product Engineer</b></Typography>
                  <Typography variant="body2" color="#000000" style={{marginTop:'0.1rem'}}>Codingmart Technologies - Internship</Typography>
                  <Typography variant="body2" color="#666666" style={{marginTop:'0.1rem'}}>Sep 2021 - Present - 7 mos</Typography>
                </Grid>
              </Grid>
              <br/><br/>
            </div>
            
          
        </Grid>

        <Grid style={{marginTop:'1.5rem',backgroundColor:'#ffffff',marginLeft:'4rem',marginRight:'4rem',borderRadius:'1rem'}}>
          <br/>
            <div style={{marginLeft:'2rem', marginTop:'1.5rem'}}>
              <div>
              <Typography variant="h5" color="#000000"><b>Education</b></Typography>
              </div>
              
              <br/>
              <Grid container direction="row" alignItems="center">
                <Grid item>
                  <SchoolIcon  style={{color:'#666666',fontSize:'2rem',marginRight:'1rem'}}/>
                </Grid>
                <Grid item>
                  <Typography variant="body2" color="#000000" style={{marginTop:'0.5rem'}}><b>Karpagam College of Engineering</b></Typography>
                  <Typography variant="body2" color="#000000" style={{marginTop:'0.1rem'}}>Bachelor of Technology - BTech, Information Technology</Typography>
                  <Typography variant="body2" color="#666666" style={{marginTop:'0.1rem'}}>2018 - 2022</Typography>
                </Grid>
              </Grid>
              <br/><br/>
            </div>
            
          
        </Grid>
        <br/><br/><br/>
      </div>
    </>
  )
}
